#!/bin/sh

echo "Waiting for postgres..."

while ! nc -z users-db 5432; do
  sleep 0.1
done

echo "Postgres started"

gunicorn --bind 0.0.0.0:5000 manage:app

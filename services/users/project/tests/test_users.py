# services/users/project/tests/test_users.py

import json
import unittest
from project.tests.base import BaseTestCase
from project.api.models import User


class TestUserService(BaseTestCase):
    def test_users_ping_success(self):
        response = self.client.get("/users/ping")
        status, message = json.loads(response.data.decode()).items()

        self.assertEqual(response.status_code, 200)
        self.assertIn("success", status)
        self.assertIn("pong!", message)

    def test_get_all_users_success(self):
        user1 = User(username="rizvan", email="rizvan@mail.com")
        user1.save()
        user2 = User(username="chalilovas", email="chalilovas@mail.com")
        user2.save()

        with self.client:
            response = self.client.get("/users")
            users = json.loads(response.data.decode())["users"]
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(users), 2)
            user1_result, user2_result = users
            self.assertEqual(user1.username, user1_result["username"])
            self.assertEqual(user2.username, user2_result["username"])

    def test_add_user_failure_missing_username(self):
        with self.client:
            response = self.client.post(
                "/users",
                data=json.dumps({
                    "email": "rizvan@mail.com",
                }),
                content_type="application/json"
            )
            self.assertEqual(response.status_code, 400)

    def test_add_user_failure_missing_email(self):
        with self.client:
            response = self.client.post(
                "/users",
                data=json.dumps({
                    "username": "chalilovas",
                }),
                content_type="application/json"
            )
            self.assertEqual(response.status_code, 400)

    def test_add_user_failure_missing_username_and_email(self):
        with self.client:
            response = self.client.post(
                "/users",
                data=json.dumps({
                }),
                content_type="application/json"
            )
            self.assertEqual(response.status_code, 400)

    def test_add_user_failure_duplicate_username(self):
        User(username="chalilovas", email="chalilovas@mail.com").save()

        with self.client:
            response = self.client.post(
                "/users",
                data=json.dumps({
                    "username": "chalilovas",
                    "email": "rizvan@mail.com"
                }),
                content_type="application/json"
            )
            self.assertEqual(response.status_code, 400)

    def test_add_user_failure_duplicate_email(self):
        User(username="chalilovas", email="chalilovas@mail.com").save()
        with self.client:
            response = self.client.post(
                "/users",
                data=json.dumps({
                    "username": "rizvan",
                    "email": "chalilovas@mail.com"
                }),
                content_type="application/json"
            )
            self.assertEqual(response.status_code, 400)

    def test_add_user_success(self):
        User(username="chalilovas", email="chalilovas@mail.com").save()

        with self.client:
            response = self.client.post(
                "/users",
                data=json.dumps({
                    "username": "rizvan",
                    "email": "rizvan@mail.com"
                }),
                content_type="application/json"
            )
            self.assertEqual(response.status_code, 201)

    def test_get_user_by_id_failure_no_id(self):
        pass

    def test_get_user_by_id_failure_invalid_id(self):
        pass

    def test_get_user_by_id_success(self):
        user = User(username="TestUser", email="test.user@mail.com")
        user.save()

        with self.client:
            response = self.client.get(f"/users/{user.id}")
            user_result = json.loads(response.data.decode())["user"]
            self.assertEqual(response.status_code, 200)
            self.assertEqual(user.username, user_result["username"])


if __name__ == '__main__':
    unittest.main()

from flask import Blueprint, request
from flask_restful import Resource, Api
from project.api.models import User
from project.api.schemas import UserSchema

users_blueprint = Blueprint("users", __name__)
api = Api(users_blueprint)

user_schema = UserSchema()
users_list_schema = UserSchema(many=True)


user_by_id_not_found_message = "User by id:{} could not be found."
generic_user_found_message = "User found."
user_exists_message = "User already exists."
user_created_message = "User was successfuly created."


class UsersPing(Resource):
    def get(self):
        """Check if users service is OK"""
        return {"status": "success", "message": "pong!"}


class UsersList(Resource):
    def get(self):
        """Get all users"""
        users = users_list_schema.dump(User.get_all())
        return {"users": users}, 200

    def post(self):
        """Add new user"""
        errors = user_schema.validate(request.get_json())
        if errors:
            return {"errors": errors}, 400

        user_request = user_schema.load(request.get_json())
        username = user_request.username
        email = user_request.email

        if User.find_existing_user(username, email):
            return {"message": user_exists_message.format(email)}, 400

        user_request.save()
        return {"message": user_created_message}, 201


class Users(Resource):
    def get(self, user_id: int):
        '''Get user by id'''

        user = User.find_by_id(user_id)

        if not user:
            return ({
                "message:": user_by_id_not_found_message.format(user_id)
            }, 404)

        return ({
            "message": generic_user_found_message,
            "user": user_schema.dump(user)
        }, 200)


api.add_resource(UsersPing, "/users/ping")
api.add_resource(UsersList, "/users")
api.add_resource(Users, "/users/<int:user_id>")

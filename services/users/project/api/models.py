from datetime import datetime
from typing import List
from project import db


class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(128), unique=True, nullable=False)
    email = db.Column(db.String(128), unique=True, nullable=False)
    active = db.Column(db.Boolean(), default=True, nullable=False)
    task_completions_goal = db.Column(db.Integer, default=0)
    creation_date = db.Column(
        db.DateTime,
        default=datetime.utcnow,
        nullable=False
    )

    @classmethod
    def get_all(cls) -> List["User"]:
        return cls.query.all()

    @classmethod
    def find_by_id(cls, _id: int) -> "User":
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_by_username(cls, username: str) -> "User":
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_existing_user(cls, username: str, email: str) -> "User":
        return User.query.filter(
            (cls.username == username) |
            (cls.email == email)
        ).first()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

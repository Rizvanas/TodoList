from project import ma
from project.api.models import User


class UserSchema(ma.ModelSchema):
    class Meta:
        model = User
        load_only = ("email",)
        dump_only = ("id", "active")
